FROM python:3.10-slim-buster

WORKDIR /python-docker

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY . .

# waiting for a public repo
#RUN set -ex; apt-get update; apt-get install -y --no-install-recommends git
#RUN git clone https://github.com/JMMC-OpenDev/searchftt-ranking.git

CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]
