# Jmmc Python Webservices

# Install container
## clone searchftt-ranking repo
git clone https://github.com/JMMC-OpenDev/searchftt-ranking.git
## Build container
docker login gricad-registry.univ-grenoble-alpes.fr
docker build -t gricad-registry.univ-grenoble-alpes.fr/osug/jmmc/jmmc-python-webservices .
docker push gricad-registry.univ-grenoble-alpes.fr/osug/jmmc/jmmc-python-webservices

# Deploy container
Please check jmmc-python-webservices-kubernetes repo to deploy service

or run it 
docker run -p5000:5000 gricad-registry.univ-grenoble-alpes.fr/osug/jmmc/jmmc-python-webservices

