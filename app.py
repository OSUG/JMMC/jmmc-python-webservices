#!/bin/env python

import json
from flask import Flask,request,jsonify
app = Flask(__name__)

version="v0.0.1"

@app.route('/')
def hello_jmmc():
    return f"""<html>
    <body>
        <h1>Welcome on the jmmc python webservice wrapper.</h1>

        <dl><dt>Version:</dt><dd>{version}</dd>
        </dl>
    </body>
    </html>
    """


#  curl -v --header 'Content-Type: application/json' --data-raw '[[1,2,3],[1,2,3]]' localhost:5000/echo 
@app.route('/echo', methods=['POST'])
def echo():
    try:
        input = request.get_json()
        return {"input": input}
    except Exception as e:
        return {
                'message': "Bad request!",
                'status': 400,
                'Error': f'{e}',
            }, 400

# SearchFTT Ranking

import sys
sys.path.append("searchftt-ranking")
from ranking import ranking_GRAVITY_UT_NGS

#ranking_GRAVITY_UT_NGS(sci_Kmag, ft_Kmag, sci_ft_dist, ao_Rmag, sci_ao_dist, ft_ao_dist):
#  ranking_GRAVITY_UT_NGS(12.0, 5.0, 3.0, 7.0, 2.0, 5.0)
#  curl -v --header 'Content-Type: application/json' --data-raw '[[12.0, 5.0, 3.0, 7.0, 2.0, 5.0]]' localhost:5000/ut_ngs_score
@app.route('/ut_ngs_score', methods=['POST'])
def ngs_score():
    try:        
        input = request.get_json()
        res = []
        for row in input:
            res.append(ranking_GRAVITY_UT_NGS(*row))
        return {"ngs_scores": res}
    except Exception as e:
        return {
                'message': "Bad request!",
                'status': 400,
                'Error': f'{e}',
            }, 400


if __name__ == "__main__":
    app.run(debug=False)

